from django.conf.urls import include, url
from .views import MyModelListView

urlpatterns = [
    url(r'^mymodels/?$', MyModelListView.as_view(), name='mymodels_list'),
]
