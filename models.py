from django.db import models

class MyModel(models.Model):
    
    class Meta:
        verbose_name = "My Model"
        verbose_name_plural = "My Models"
        
    info = models.TextField(
                    default = "",
                    verbose_name = "My Model Info",
                    help_text = """
                        Info about this My Model object
                        """,
                    null = False,
                    blank = False,
                )    
    def __str__(self):
        return self.info
